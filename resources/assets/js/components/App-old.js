import React, { Component } from 'react';
import axios from 'axios';

import ReactDOM from 'react-dom'

import './App.css';
import ImgList from './ImgList';
import SearchForm from './SearchForm';
//import cred from './cred.js';
import ReactPaginate from 'react-paginate';
import Progress from 'react-progress-2';
import 'react-progress-2/main.css';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgs: [],
      loadingState: true,
      searchText:'wine',
      pageCount: 1,
      perPage: 25,
      currentPage: 1,
    };
     this.onReSearchChange = this.onReSearchChange.bind(this)
     this.performSearch = this.performSearch.bind(this)
     this.handlePageClick=this.handlePageClick.bind(this)
     this.handleSubmit=this.handleSubmit.bind(this)
     this.onSearchChange=this.onSearchChange.bind(this)
  }

    onReSearchChange(data){
    
      this.setState({ imgs: data.data.results, 
                       loadingState: false,
                       //pageCount: data.data.total_pages,
                       pageCount:200
                        
                        })
       window.scrollTo(0, 0);
       Progress.hide();
    }
    async onSearchChange (event) {
      
        await Promise.resolve(this.setState(() => (
          { currentPage: page,pageCount:1, [event.target.searchText]: event.target.value }
          )));
        console.log("search change function ",this.state.currentPage);
      }
   async handleSubmit (e) {
        //e.preventDefault();
       
        //console.log(e.target.value)
        console.log('query =',e)
          await Promise.resolve(this.setState(() => (
          { currentPage: 1,pageCount:1,searchText:e}
          )));
        
         this.performSearch();
  
      };
  componentDidMount() {
    const page = this.state.currentPage;
    console.log('page',page)
    this.setState(() => ({ currentPage: page ? page : 1 }));
    this.performSearch();
  }

  async handlePageClick(data) {
    const page = data.selected >= 0 ? data.selected+1  : 0;
    console.log('after click',page)
     await Promise.resolve(this.setState(() => ({ currentPage: page })));
     console.log("handle function ",this.state.currentPage);
     //this.performSearch();
  }

  
  performSearch () {
    Progress.show();
      console.log("in function ",this.state.currentPage);
    axios
      .get(
        'https://api.unsplash.com/search/photos?page='+this.state.currentPage
        +'&per_page=200&query='+this.state.searchText+'&client_id='+process.env.MIX_UNSPLASH_API_KEY+''
      )
      .then(data => {
        this.onReSearchChange(data)
        
      })
      .catch(err => {
        Progress.hide();
        console.log('Error happened during fetching!', err);
      });
  };

  render() {
    return (     

      <div>
        <div className="main-header">
          <div className="inner">
            <h1 className="main-title">ImageSearch</h1>
            <SearchForm onSearch={this.handleSubmit} />
          </div>
        </div>
        <div className="main-content">
          {this.state.loadingState
            ? <p>Loading</p>
            : <ImgList data={this.state.imgs} />}
        </div>

          <div>
              <Progress.Component
                style={{ background: '#99999978', height: '5px' }}
                thumbStyle={{ background: '#5900b3', height: '5px' }}
              />

              {this.state.pageCount > 1 ?
              <nav aria-label="Page navigation example">
              <ReactPaginate
                pageCount={this.state.pageCount}
          initialPage={this.state.currentPage - 1}
          forcePage={this.state.currentPage - 1}
          pageRangeDisplayed={4}
          marginPagesDisplayed={2}
                previousLabel="&#x276E;"
                nextLabel="&#x276F;"
                disabledClassName="disabled"
                onPageChange={this.handlePageClick}
                disableInitialCallback={true}

                 breakClassName={'page-item'}
                 breakLinkClassName={'page-link'}
     containerClassName={'pagination justify-content-center'}
     pageClassName={'page-item'}
     pageLinkClassName={'page-link'}
     previousClassName={'page-item'}
     previousLinkClassName={'page-link'}
     nextClassName={'page-item'}
     nextLinkClassName={'page-link'}
     activeClassName={'active'}

              />
              </nav>
              :null }
            
            </div>


      </div>
    );
  }
}
ReactDOM.render(<App />, document.getElementById('app'))