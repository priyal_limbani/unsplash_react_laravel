import React from 'react';
import LazyLoad from 'react-lazyload';

const Img = props =>
	<li>
		<a href={props.link}>
		<LazyLoad height={200} 
		       delayTime={2000}
		       effect="blur"
		       threshold ={800}
		       debounce={false}
      			offsetVertical={500}
      			throttle={600}>
			<img src={props.url} alt="Unsplash Image here" />
		</LazyLoad>
		</a>
		<p>
			Photo by
			<a href={props.user}>{props.name}</a>
			<a href={props.link}> See on Unsplash</a>
		</p>
	</li>
	
export default Img;