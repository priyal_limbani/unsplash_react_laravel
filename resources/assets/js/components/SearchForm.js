import axios from 'axios'
import React, { Component } from 'react'

class SearchForm extends Component {
  constructor (props) {
    super(props)

    this.state = {
     //searchText:'',
    }

    this.onSearchChange = this.onSearchChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  onSearchChange (event) {
  	console.log('onserchchznhe');
    this.setState({
      [event.target.searchText]: event.target.value
    })
    console.log(event)
  }
handleSubmit (e) {
		e.preventDefault();
		this.props.onSearch(this.query.value);
		//e.currentTarget.reset();
	};

  render () {
    return (
      <form className="search-form" onSubmit={this.handleSubmit}>
				<label className="is-hidden" htmlFor="search">Search</label>
				<input
					type="search"
					onChange={this.onSearchChange}
					ref={input => (this.query = input)}
					name="search"
					placeholder="Search..."
				/>
				<button type="submit" id="submit" className="search-button">
					<i className="material-icons icn-search">search</i>
				</button>
			</form>
    )
  }
}

export default SearchForm


